# 作业



```java
//第一题
import java.util.Scanner;

public class zuoye1 {
    public static void main(String[] args) {
//        判断一个字符数据是否是数字字符 
        Scanner sc = new Scanner(System.in);
        char a = sc.next().charAt(0);//charAT(0)取首位
        if ((int)a>=48 && (int)a<=57){
            System.out.println("是数字字符");
        }else {
            System.out.println("不是数字字符");
        }
    }
}


```

```java
//第二题
import java.util.Scanner;

public class zuoye2 {
//    需要判断一个字符是否是字母字符，首先需要提供一个字符数据
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char a = sc.next().charAt(0);
        if ((int)a>=65 && (int)a<=90){
            System.out.println("是字母字符");
        } else if ((int)a>=97 && (int)a<=122) {
            System.out.println("是字母字符");
        }else {
            System.out.println("不是字母字符");
        }
    }
}


```

```java
//第三题
import java.util.Scanner;

public class zuoye3 {
    public static void main(String[] args) {
//        判断指定的年份是否为闰年，请使用键盘录入**
//**分析：
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        if (a%4==0){
            System.out.println(a+"是闰年");
        }else {
            System.out.println(a+"不是闰年");
        }
    }
}


```

```java
//第四题
import java.util.Scanner;

public class zuoye4 {
    public static void main(String[] args) {
//        1×1×*1 + 5*×5×*5 + 3×*3×3 = 153; 就是水仙花数
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = a/100;
        int c = a/10%10;
        int d = a%10;
        int e = b*b*b+c*c*c+d*d*d;
        if (a>99 && a<1000){
            if (e==a){
                System.out.println(a+"是水仙花数");
            }else {
                System.out.println(a+"不是水仙花数");
            }
        }
    }
}


```

```java
//第五题
import java.util.Scanner;

public class zuoye5 {
    public static void main(String[] args) {
//        五位数的回文数是指最高位和最低位相等，次高位和次低位相等。如：12321  23732  56665
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = a/10000;//万
        int c = a/1000%10;//千
        int d = a/100%10;//百
        int e = a/10%10;//十
        int f = a%10;//个
        if (a>9999 & a<100000){
            if (b == f & c==e){
                System.out.println(a+"是回文数");
            }else {
                System.out.println(a+"不是回文数");
            }
        }
    }
}


```
